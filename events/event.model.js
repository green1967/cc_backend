const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    calendar_id: { type: String, required: true },
    user_id: { type: String, required: true },
    startDate: { type: Date, required: true},
    endDate: { type: Date, required: true},
    title: { type: String, required: true},
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Event', schema);