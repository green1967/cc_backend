﻿const config = require('config.json');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const Event = db.Event;


module.exports = {
    create,
    getListbyUserID,
    update,
    getAll,
    exportJson,
    delete: _delete,
};


async function authenticate({ name, password }) {
    const user = await User.findOne({ name });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret);
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function create(userParam) {

    const event = new Event(userParam)
    // save user
    await event.save();
}

async function getListbyUserID(userParam) {
    return await Event.find({user_id: userParam.user_id, calendar_id: userParam.calendar_id});
}

async function getAll() {
    return await Event.find();
}

async function update(id, userParam) {
    const event = await Event.findById(id);

    // validate
    if (!event) throw 'Event not found';
    if (event.user_id !== userParam.user_id) {
        throw 'You are not the event owner!';
    }

     // copy userParam properties to user
    Object.assign(event, userParam);

    await event.save();
}

async function _delete(id) {
    await Event.findByIdAndRemove(id);
}

async function exportJson(userParam) {    
    return await Event.find({user_id: userParam.user_id, calendar_id: userParam.calendar_id});    
}
