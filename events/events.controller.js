const express = require('express');
const router = express.Router();
const eventService = require('./event.service');
const fs = require('fs');


// routes
router.get('/add-event', create);
router.get('/download', downloadJSON);
router.get('/get-events-by-user', getByUserIdCalendarId);
router.get('/get-all', getAll);
router.get('/:id', update);
router.get('/delete-by-id/:id', _delete);


// router.post('/login', authenticate);
// router.post('/register', register);
// router.get('/get-video', getVideosByUser);
// router.get('/get-pro', getPro);
// router.post('/upload', upload.single('avatar'), uploadFile)
// router.get('/', getAll);
// router.get('/current', getCurrent);
// router.get('/:id', getById);
// router.put('/:id', upload.single('avatar'), update);



module.exports = router;




function create(req, res, next) {
//console.log('req.body=' + req.body);
    eventService.create(req.query)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getByUserIdCalendarId(req, res, next) {
    eventService.getListbyUserID(req.query)
        .then((events) => res.json({events}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    eventService.getAll()
        .then((events) => res.json({events}))
        .catch(err => next(err))
}

function update(req, res, next) {
    eventService.update(req.params.id, req.query)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function downloadJSON(req, res, next) {
    eventService.getListbyUserID(req.query)
        .then((events) => {
            const initDate = new Date('2020-06-12T08:00:00+03:00');
            const fileName = Date.parse(new Date())+".json";
            const result = [];
            events.forEach((item) => { 
                let obj = {};  
                obj.title = item.title;
                obj.start = (Date.parse(item.startDate) - Date.parse(initDate))/1000/60;
                obj.duration = (Date.parse(item.endDate) - Date.parse(item.startDate))/1000/60;
                result.push(obj);
            }); 
            let data = JSON.stringify(result, null, 2);
            fs.writeFileSync(fileName, data);
            res.download(fileName)})
        .catch(err => next(err));
}

function _delete(req, res, next) {
    eventService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}
